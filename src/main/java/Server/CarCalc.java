package Server;

import Model.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CarCalc extends Remote {

    public double coumputeTax(Car c) throws RemoteException;
    public double computeSellingPrice(Car c) throws RemoteException;
}
