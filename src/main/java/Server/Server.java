package Server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {

    public static void main(String[] args) throws RemoteException {
        try {
            CarService carService = new CarService();
            CarCalc stub = (CarCalc) UnicastRemoteObject.exportObject(carService, 0);

            Registry registry = LocateRegistry.createRegistry(5099);
            registry.rebind("CarCalc", stub);
            System.out.print("Server up");
        }
        catch (Exception ex){
            System.err.print("Server error");
            ex.printStackTrace();
        }
    }
}
