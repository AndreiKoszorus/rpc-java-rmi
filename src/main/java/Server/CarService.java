package Server;

import Model.Car;

import java.rmi.RemoteException;

public class CarService implements CarCalc {
    public CarService(){
        super();
    }
    public double coumputeTax(Car c) throws RemoteException {
        if (c.getEngineSize() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineSize() > 1601) sum = 18;
        if(c.getEngineSize() > 2001) sum = 72;
        if(c.getEngineSize() > 2601) sum = 144;
        if(c.getEngineSize() > 3001) sum = 290;
        return c.getEngineSize() / 200.0 * sum;
    }

    public double computeSellingPrice(Car c) throws RemoteException {
        double sellingPrice = 0;
        if(2018-c.getYear() < 7){
            sellingPrice = c.getPurchasingPrice()- c.getPurchasingPrice()/7 * (2018-c.getYear());
        }
        return sellingPrice;
    }
}

