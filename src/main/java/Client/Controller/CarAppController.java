package Client.Controller;

import Model.Car;
import Server.CarCalc;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;



import java.io.FileInputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CarAppController extends Application {


    @FXML
    private TextField yearField;
    @FXML
    private TextField engineField;
    @FXML
    private TextField priceField;
    @FXML
    private TextField resultField;

    private Registry registry;

    private CarCalc carCalc;

    public CarAppController(){
        try {
            registry = LocateRegistry.getRegistry("localhost",5099);
            carCalc = (CarCalc) registry.lookup("CarCalc");
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }


    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "E://SD//AK//Assignement22//src//main//java//Client//View//GUI.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        //System.out.println(username);

        // Create the Pane and all Details
        AnchorPane root = (AnchorPane) loader.load(fxmlStream);
        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        primaryStage.setScene(scene);
        // Set the Title to the Stage
        primaryStage.setTitle("Login");
        // Display the Stage
        primaryStage.show();
    }

    public void computeTax() throws RemoteException {
        int year = Integer.parseInt(yearField.getText());
       int engineSize = Integer.parseInt(engineField.getText());
        double purchasingPrice = Double.parseDouble(priceField.getText());

        Car c = new Car(year,engineSize,purchasingPrice);
        double result = carCalc.coumputeTax(c);

        resultField.setText(""+result);

    }

    public void calcPrice() throws RemoteException{
        int year = Integer.parseInt(yearField.getText());
        int engineSize = Integer.parseInt(engineField.getText());
        double purchasingPrice = Double.parseDouble(priceField.getText());

        Car c = new Car(year,engineSize,purchasingPrice);
        double result = carCalc.computeSellingPrice(c);

        resultField.setText(""+result);

    }


}
